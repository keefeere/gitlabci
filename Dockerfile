FROM openjdk:8-jre-alpine

RUN addgroup -S devops && adduser -S devops -G devops

COPY /app/target/*.jar /opt/demo.jar


EXPOSE 8080:8080


ENTRYPOINT ["java"]
CMD ["-jar","/opt/demo.jar"]
