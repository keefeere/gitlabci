GitLabCI Pipeline that build Spring Boot java application
==========

* Build docker images throug docker socket binding
* Pass Nexus credentials by GitLab Variables
* anchors in  .gitlab-ci.yaml
* SonarCube code validation
* Deploy image to docker container