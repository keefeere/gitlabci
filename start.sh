#!/bin/bash

. /vagrant/gitlabtoken.sh

      docker stop $(docker ps -a -q)
      docker rm $(docker ps -a -q)
      docker rmi -f $(docker images -q)


docker run -d --name gitlab-runner --restart always \
     -v /srv/gitlab-runner/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     gitlab/gitlab-runner:latest

docker run --rm -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --executor "docker" \
  --docker-image "docker:19.03.12" \
  --url "https://gitlab.com/" \
  --registration-token ${GITLABTOKEN} \
  --description "docker-runner" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
  --tag-list "docker,vagrant" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"


docker volume create --name sonarqube_data
docker volume create --name sonarqube_logs
docker volume create --name sonarqube_extensions

docker pull sonarqube:8.7.1-community

docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true -p 9000:9000 sonarqube:8.7.1-community